<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-datetime-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DateTimeFormat\DateTimeFormat;
use PHPUnit\Framework\TestCase;

/**
 * DateTimeFormatTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DateTimeFormat\DateTimeFormat
 *
 * @internal
 *
 * @small
 */
class DateTimeFormatTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var DateTimeFormat
	 */
	protected DateTimeFormat $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
