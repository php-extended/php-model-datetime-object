<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-datetime-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DateTimeFormat;

use ReflectionException;

/**
 * DateTimeFormat class file.
 * 
 * This class represents date and time formats for a relational model.
 * 
 * @author Anastaszor
 */
enum DateTimeFormat : string implements DateTimeFormatInterface
{
	
	/**
	 * Finds a DateTimeFormat according to the use of date, time, microtime
	 * and timezone values. If no right combination exists, a default 
	 * TIMESTAMP format will be returned.
	 * 
	 * @param boolean $usesDate
	 * @param boolean $usesTime
	 * @param boolean $usesMicrotime
	 * @param boolean $usesTimezone
	 * @return DateTimeFormatInterface
	 * @throws ReflectionException
	 */
	public static function findBy(bool $usesDate, bool $usesTime, bool $usesMicrotime, bool $usesTimezone) : DateTimeFormatInterface
	{
		if(false === $usesDate && false === $usesTime)
		{
			$usesDate = $usesTimezone;
			$usesTime = $usesMicrotime;
		}
		
		foreach(static::cases() as $dateTimeFormat)
		{
			/** @var DateTimeFormat $dateTimeFormat */
			if($dateTimeFormat->usesDate() === $usesDate
				&& $dateTimeFormat->usesTime() === $usesTime
				&& $dateTimeFormat->usesMicrotime() === $usesMicrotime
				&& $dateTimeFormat->usesTimezone() === $usesTimezone
			) {
				return $dateTimeFormat;
			}
		}
		
		return DateTimeFormat::TIMESTAMP;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DateTimeFormat\DateTimeFormatInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $this === $object;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DateTimeFormat\DateTimeFormatInterface::usesDate()
	 */
	public function usesDate() : bool
	{
		return \strpos($this->value, 'date') !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DateTimeFormat\DateTimeFormatInterface::usesTime()
	 */
	public function usesTime() : bool
	{
		return \strpos($this->value, 'time') !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DateTimeFormat\DateTimeFormatInterface::usesMicrotime()
	 */
	public function usesMicrotime() : bool
	{
		return \strpos($this->value, 'micro') !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DateTimeFormat\DateTimeFormatInterface::usesTimestamp()
	 */
	public function usesTimestamp() : bool
	{
		return \strpos($this->value, 'stamp') !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DateTimeFormat\DateTimeFormatInterface::usesTimezone()
	 */
	public function usesTimezone() : bool
	{
		return \strpos($this->value, 'tz') !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DateTimeFormat\DateTimeFormatInterface::getEarlyestDateTime()
	 */
	public function getEarlyestDateTime() : string
	{
		return match($this)
		{
			DateTimeFormat::TIME => \date('Y-m-d').'T00:00:00+00:00',
			DateTimeFormat::TIME_WITH_TIMEZONE => \date('Y-m-d').'T00:00:00-12:00',
			DateTimeFormat::DATE => '0000-01-01T00:00:00+00:00',
			DateTimeFormat::DATE_WITH_TIMEZONE => '0000-01-01T00:00:00-12:00',
			DateTimeFormat::TIMESTAMP => '1970-01-01T00:00:00+00:00',
			DateTimeFormat::TIMESTAMP64 => '1970-01-01T00:00:00+00:00',
			DateTimeFormat::DATETIME => '0000-01-01T00:00:00+00:00',
			DateTimeFormat::DATETIME_WITH_TIMEZONE => '0000-01-01T00:00:00-12:00',
			DateTimeFormat::MICROTIME => \date('Y-m-d').'T00:00:00+00:00',
			DateTimeFormat::MICROTIME_WITH_TIMEZONE => \date('Y-m-d').'T00:00:00-12:00',
			DateTimeFormat::DATEMICROTIME => '0000-01-01T00:00:00+00:00',
			DateTimeFormat::DATEMICROTIME_WITH_TIMEZONE => '0000-01-01T00:00:00-12:00',
		};
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DateTimeFormat\DateTimeFormatInterface::getLatestDateTime()
	 */
	public function getLatestDateTime() : string
	{
		return match($this)
		{
			DateTimeFormat::TIME => \date('Y-m-d').'T23:59:59+00:00',
			DateTimeFormat::TIME_WITH_TIMEZONE => \date('Y-m-d').'T23:59:59+12:00',
			DateTimeFormat::DATE => '9999-12-31T00:00:00+00:00',
			DateTimeFormat::DATE_WITH_TIMEZONE => '9999-12-31T00:00:00+12:00',
			DateTimeFormat::TIMESTAMP => '2038-01-19T03:14:07+00:00',
			DateTimeFormat::TIMESTAMP64 => '9999-12-31T23:59:59+00:00',
			DateTimeFormat::DATETIME => '9999-12-31T23:59:59+00:00',
			DateTimeFormat::DATETIME_WITH_TIMEZONE => '9999-12-31T23:59:59+12:00',
			DateTimeFormat::MICROTIME => \date('Y-m-d').'T23:59:59+00:00',
			DateTimeFormat::MICROTIME_WITH_TIMEZONE => \date('Y-m-d').'T00:00:00+12:00',
			DateTimeFormat::DATEMICROTIME => '9999-12-31T23:59:59+00:00',
			DateTimeFormat::DATEMICROTIME_WITH_TIMEZONE => '9999-12-31T23:59:59+12:00',
		};
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DateTimeFormat\DateTimeFormatInterface::getPhpDateTimeFormat()
	 */
	public function getPhpDateTimeFormat() : string
	{
		return match($this)
		{
			DateTimeFormat::TIME => 'H:i:s',
			DateTimeFormat::TIME_WITH_TIMEZONE => 'H:i:s O',
			DateTimeFormat::DATE => 'Y-m-d',
			DateTimeFormat::DATE_WITH_TIMEZONE => 'Y-m-d O',
			DateTimeFormat::TIMESTAMP => 'U',
			DateTimeFormat::TIMESTAMP64 => 'U',
			DateTimeFormat::DATETIME => 'Y-m-d H:i:s',
			DateTimeFormat::DATETIME_WITH_TIMEZONE => 'Y-m-d H:i:s O',
			DateTimeFormat::MICROTIME => 'H:i:s.v',
			DateTimeFormat::MICROTIME_WITH_TIMEZONE => 'H:i:s.v O',
			DateTimeFormat::DATEMICROTIME => 'Y-m-d H:i:s.v',
			DateTimeFormat::DATEMICROTIME_WITH_TIMEZONE => 'Y-m-d H:i:s.v O',
		};
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DateTimeFormat\DateTimeFormatInterface::mergeWith()
	 * @throws ReflectionException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function mergeWith(DateTimeFormatInterface $other) : DateTimeFormatInterface
	{
		if($this->usesTimestamp() && $other->usesTimestamp())
		{
			if(DateTimeFormat::TIMESTAMP64 === $this)
			{
				return $this;
			}
			
			return $other;
		}
		
		$useTimestamp = false;
		if($this->usesTimestamp() || $other->usesTimestamp())
		{
			$useTimestamp = true;
		}
		
		return static::findBy(
			$this->usesDate() || $other->usesDate() || $useTimestamp,
			$this->usesTime() || $other->usesTime() || $useTimestamp,
			$this->usesMicrotime() || $other->usesMicrotime(),
			$this->usesTimezone() || $other->usesTimezone(),
		);
	}
	
	case TIME = 'time';
	case TIME_WITH_TIMEZONE = 'timetz';
	case DATE = 'date';
	case DATE_WITH_TIMEZONE = 'datetz';
	case TIMESTAMP = 'timestamp';
	case TIMESTAMP64 = 'timestamp64';
	case DATETIME = 'datetime';
	case DATETIME_WITH_TIMEZONE = 'datetimetz';
	case MICROTIME = 'microtime';
	case MICROTIME_WITH_TIMEZONE = 'microtimetz';
	case DATEMICROTIME = 'datemicrotime';
	case DATEMICROTIME_WITH_TIMEZONE = 'datemicrotimetz';
	
}
